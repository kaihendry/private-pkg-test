/**
 * return a random state in South Africa
 */

 module.exports = () => {
    return ["Eastern Cape", "Free State", "Gauteng", "KwaZulu-Natal", "Limpopo", "Mpumalanga", "North West", "Northern Cape", "Western Cape"][Math.floor(Math.random() * 9)]
 };
